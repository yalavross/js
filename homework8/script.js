const input = document.querySelector('.input-field');
const form = document.getElementById('form-id')
console.log(input)
input.addEventListener('focus', function (e) {
    e.preventDefault();
    input.style.border = '3px solid green';
})

input.addEventListener('blur', function () {
    input.style.border = '3px solid #7c7a7a';
    const inputValue = document.querySelector('.input-field').value;
    const price = document.createElement('span');
    const close = document.createElement('button');
    close.className = 'delete-span';
    const error = document.createElement('span');
    if (inputValue <= 0) {
        error.className = 'error-span';
        error.appendChild(document.createTextNode('Please enter correct price'));
        input.style.border = '3px solid #dc143c';
        form.appendChild(error);
        span = document.getElementByTagName = 'span';
        close.appendChild(document.createTextNode(`x`))
        error.appendChild(close);
        price.style.display = 'none';

    } else {
        price.className = 'price-span';
        price.appendChild(document.createTextNode(`Current price: ${inputValue}`))
        form.prepend(price);
        input.id = 'price';
        close.appendChild(document.createTextNode(`x`))
        price.appendChild(close);
        error.style.display = 'none';
    }
})